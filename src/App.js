import React, { Component } from 'react';
import ReactDom from 'react-dom';
import {BrowserRouter , Route , Switch} from "react-router-dom";



// start styling here 
import './assets/css/flaticon.css';
import './assets/css/boxicons.css';
import './assets/css/animate.css';
import './assets/css/magnific-popup.css';
import './assets/css/meanmenu.css';
import './assets/css/nice-select.css';
import './assets/css/odometer.css';
import './assets/css/owl.carousel.min.css';
import './assets/css/owl.theme.default.min.css';
import './assets/css/responsive.css';
import 'bootstrap/dist/css/bootstrap.min.css';
import './index.css';
import './assets/css/style.css';
import './assets/css/rtl.css';


// import component
import Navbar from './Components/Navbar/Navbar';
import Index from './Components/Index/Index';
import About from './Components/About/About';
import Shop from './Components/Shop/Shop';
import Cart from './Components/CartPage/Cart';
import Contact from './Components/Contact/Contact';
import page404 from './Components/404Page/Page404'; 

function App() {
  return (
    <BrowserRouter>
      <div className="App">
        <Navbar/>
        <Switch>
        <Route exact path="/" component={Index}/>
        <Route exact path="/About" component={About}/>
        <Route exact path="/Shop" component={Shop} /> 
        <Route exact path="/Cart" component={Cart} />
        <Route exact path="/Contact" component={Contact} />
        <Route exact component={page404}/>
        </Switch>
      </div>
      </BrowserRouter>
  );
}

export default App;
