import React from 'react';

class Page404 extends React.Component {

	render(){
		return(
<div class="error-area">
			<div class="d-table">
				<div class="d-table-cell">
					<div class="error-content-wrap">
						<h1><span class="a">4</span> <span class="red">0</span> <span class="b">4</span> </h1>
						<h3>Oops! Page Not Found</h3>
						<p>The page you were looking for could not be found.</p>
						<a href="/" class="default-btn two">
							Return To Home Page
						</a>
					</div>
				</div>
			</div>
		</div>

			)
		}
	}

export default Page404;