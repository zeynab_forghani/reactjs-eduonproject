import React from 'react';
import Title from '../About/TitleArea/Title';
import Education from './EducationArea/Education';
import Affordable from './AffordableArea/Affordable';
import Events from './EventArea/Event';
import Teacher from './TeachersArea/Teachers';
import Footer from '../Footer/Footer';

class About extends React.Component {
    render() {
        return (
            <div className="About">
                <Title />
                <Education />
                <Affordable/>
                <Events/>
                <Teacher/>
                <Footer/>
            </div>
        )
    }
}


export default About;