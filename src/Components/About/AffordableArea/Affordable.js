import React from 'react';
import AffordableList from './AffordableList';

class Affordable extends React.Component {
    render() {
        return (

            <div className="fgdgd">

                <section class="affordable-area f5f6fa-bg-color pt-100 pb-70">
                    <div class="container">
                        <div class="section-title">
                            <span>مقرون به صرفه ما</span>
                            <h2>سود شما با Eduon است</h2>
                        </div>

                        <div class="row">
                            <AffordableList
                                icon="flaticon-investment"
                                title="در وقت و هزینه خود صرفه جویی کنید"
                            />

                            <AffordableList
                                icon="flaticon-balance"
                                title="تعادل را با زندگی متعادل کنید"
                            />

                            <AffordableList
                                icon="flaticon-online-education"
                                title="دانش ارزشمندی کسب کنید"
                            />

                            <AffordableList
                                icon="flaticon-route"
                                title="آنچه را که شروع کرده اید تمام کنید"
                            />
                        </div>
                    </div>
                </section>
            </div>


        )
    }
}


export default Affordable;