import React from 'react';

class Affordable extends React.Component {

    render() {
        return (

         
                <div class="col-lg-3 col-sm-6">
                    <div class="single-affordable one">
                        <i class={this.props.icon}></i>

                        <h3>{this.props.title}</h3>
                    </div>
                </div>
            

        )
    }
}

export default Affordable;