import React from 'react';
import EducationList from './EducationList';

class Education extends React.Component {

state = {
    items:[
        {
            toptitle:"آموزش برای همه" ,
            title1:"چرا یک دوره آنلاین با EDUON ایجاد" ,
            title2:" کنید" ,
            title3:"؟" ,
            paragraph1:"لورم ایپسوم متن ساختگی با تولید سادگی نامفهوم از صنعت چاپ و با استفاده از طراحان گرافیک است چاپگرها و متون بلکه روزنامه و مجله در ستون و سطرآنچنان که لازم است" ,
            paragraph2:"لورم ایپسوم متن ساختگی با تولید سادگی نامفهوم از صنعت چاپ و با استفاده از طراحان گرافیک است" ,
            li1:"همه چیز نامحدود " ,
            li2 : "بدون هزینه معامله " ,
            li3:"بدون سردرد فنی " ,
            li4:"پشتیبانی در سطح جهانی از افراد " ,
            buttonText : "مشاهده دوره ها"
 
        }
    ]
}
    render(){
        return(

            <div className="Banner">
                <EducationList items={this.state.items} />
            </div>

        )
    }
}


export default Education;