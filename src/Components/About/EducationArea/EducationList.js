import React from 'react';
import educationImg2 from '../../../assets/img/education-img-2.jpg';
import educationImg3 from '../../../assets/img/education-img-3.jpg';
import educationImg4 from '../../../assets/img/education-img-4.jpg';
import educationShap1 from '../../../assets/img/education-shape-1.jpg';
import educationShap2 from '../../../assets/img/education-shape-2.png';




class Education extends React.Component {
    render() {
        const { items } = this.props;
        const educationList = items.map(item => {
            return (
                <div className="Education">
                    <section class="education-area-two ptb-100">
                        <div class="container">
                            <div class="row">
                                <div class="col-lg-6">
                                    <div class="education-content">
                                        <span class="top-title">{item.toptitle}</span>
                                        <h2>{item.title1}<span>{item.title2}</span>{item.title3}</h2>
            <p>{item.paragraph1}</p>
            <p>{item.paragraph2}</p>

                                        <div class="row">
                                            <div class="col-lg-6">
                                                <ul>
                                                    <li>
                                                        <i class="bx bx-check"></i>
                                                {item.li1}
										</li>
                                                    <li>
                                                        <i class="bx bx-check"></i>
											{item.li2}
										</li>
                                                </ul>
                                            </div>

                                            <div class="col-lg-6">
                                                <ul>
                                                    <li>
                                                        <i class="bx bx-check"></i>
											{item.li3}
										</li>
                                                    <li>
                                                        <i class="bx bx-check"></i>
											{item.li4}
										</li>
                                                </ul>
                                            </div>
                                        </div>

                                        <a href="courses.html" class="default-btn">
                                        {item.buttonText}
							</a>
                                    </div>
                                </div>

                                <div class="col-lg-6">
                                    <div class="education-img-wrap">
                                        <div class="education-img-2">
                                            <img src={educationImg2} alt="Image" />
                                        </div>

                                        <div class="education-img-3">
                                            <img src={educationImg3} alt="Image" />
                                        </div>

                                        <div class="education-img-4">
                                            <img src={educationImg4} alt="Image" />
                                        </div>

                                        <div class="education-shape-1">
                                            <img src={educationShap1} alt="Image" />
                                        </div>

                                        <div class="education-shape-2">
                                            <img src={educationShap2} alt="Image" />
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </section>
                </div>


            )
        })




        return (

            <div>
                {educationList}
            </div>
        )
    }
}


export default Education;
