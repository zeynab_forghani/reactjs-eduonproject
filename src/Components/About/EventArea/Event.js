import React from 'react';
import eventImg1 from '../../../assets/img/event-img/event-shape-1.png';
import eventImg2 from '../../../assets/img/event-img/event-shape-2.png';
import eventImg5 from '../../../assets/img/event-img/event-img-5.png';


class Events extends React.Component {

	render(){
		return(
<section class="event-area-two event-area-style ptb-100">
			<div class="container">
				<div class="row align-items-center">
					<div class="col-lg-6">
						<div class="event-img">
							<img src={eventImg5} alt="Image" />

							<div class="video-content">
								<a href="https://www.youtube.com/watch?v=iLS_YP1uEK8" class="video-btn popup-youtube">
									<i class="flaticon-play-button"></i>
								</a>
							</div>

							<div class="event-shape-1 rotated">
								<img src={eventImg1} alt="Image" />
							</div>

							<div class="event-shape-2">
								<img src={eventImg2} alt="Image" />
							</div>
						</div>
					</div>

					<div class="col-lg-6">
						<div class="section-title">
							<span>تجربه یادگیری</span>
							<h2>چگونه می توانم به شما کمک کنم؟</h2>
						</div>

						<div class="row">
							<div class="col-lg-6 col-sm-6">
								<div class="single-tutor one">
									<i class="flaticon-instructor-1"></i>
									<h3>چهره به چهره یادگیری</h3>
									<p>لورم ایپسوم متن ساختگی با تولید سادگی نامفهوم از صنعت چاپ و با استفاده از طراحان گرافیک است</p>
								</div>
							</div>
		
							<div class="col-lg-6 col-sm-6">
								<div class="single-tutor two">
									<i class="flaticon-instructor"></i>
									<h3>مربی باتجربه</h3>
									<p>لورم ایپسوم متن ساختگی با تولید سادگی نامفهوم از صنعت چاپ و با استفاده از طراحان گرافیک است</p>
								</div>
							</div>
		
							<div class="col-lg-6 col-sm-6">
								<div class="single-tutor three">
									<i class="flaticon-certificate-1"></i>
									<h3>گواهینامه بین المللی</h3>
									<p>لورم ایپسوم متن ساختگی با تولید سادگی نامفهوم از صنعت چاپ و با استفاده از طراحان گرافیک است</p>
								</div>
							</div>
		
							<div class="col-lg-6 col-sm-6">
								<div class="single-tutor four">
									<i class="flaticon-consulting"></i>
									<h3>پشتیبانی آنلاین 24/7</h3>
									<p>لورم ایپسوم متن ساختگی با تولید سادگی نامفهوم از صنعت چاپ و با استفاده از طراحان گرافیک است</p>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</section>

			)
		}
	}

export default Events;