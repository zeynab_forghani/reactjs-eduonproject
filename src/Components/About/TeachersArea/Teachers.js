import React from 'react';
import TeacherList from './TeachersList';
import teacherImg1 from '../../../assets/img/teachers-img/teachers-img-1.jpg';
import teacherImg2 from '../../../assets/img/teachers-img/teachers-img-2.jpg';
import teacherImg3 from '../../../assets/img/teachers-img/teachers-img-3.jpg';
import teacherImg4 from '../../../assets/img/teachers-img/teachers-img-4.jpg';


class Teacher extends React.Component {
    render() {
        return (

            <section class="teachers-area ebeef5-bg-color pt-100">
                <div class="container">
                    <div class="section-title">
                        <span>معلم های ما</span>
                        <h2>معلمان بین المللی ما</h2>
                    </div>
                    <div class="row">

                        <TeacherList

                            src={teacherImg1}
                            name="ارل مک گوان"
                            job="نرم افزار و IT"

                        />

                        <TeacherList

                            src={teacherImg2}
                            name="ارل مک گوان"
                            job="نرم افزار و IT"

                        />

                        <TeacherList

                            src={teacherImg3}
                            name="ارل مک گوان"
                            job="نرم افزار و IT"

                        />

                        <TeacherList

                            src={teacherImg4}
                            name="ارل مک گوان"
                            job="نرم افزار و IT"

                        />

                    </div>
                </div>
            </section>
        )
    }
}

export default Teacher;