import React from 'react';
import {Link} from "react-router-dom";

class Title extends React.Component {
    render(){
        return(

            <div className="Title">
				<div class="page-title-area bg-1">
			<div class="container">
				<div class="page-title-content">
					<h2>درباره ما</h2>
					<ul>
						<li>
							<Link to="/">
								خانه 
							</Link>
						</li>
						
						<li class="active">درباره ما</li>
					</ul>
				</div>
			</div>
		</div>
			</div>

        )
    }
}


export default Title;
