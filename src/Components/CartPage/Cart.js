import React from 'react';
import Title from './TitleArea/Title';
import CartList from './Cart/CartList';
import Footer from '../Footer/Footer';
class Cart extends React.Component {
    render (){
        return(
            <div className="Cart">
                <Title/>
                <CartList/>
                <Footer/>
            </div>
            
        )
    }
}


export default Cart;