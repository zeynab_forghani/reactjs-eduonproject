import React from 'react';
import Right from './Right';
import Left from './Left';
import cartImg1 from '../../../assets/img/cart/cart-img-1.jpg';
import cartImg2 from '../../../assets/img/cart/cart-img-2.jpg';
import cartImg3 from '../../../assets/img/cart/cart-img-3.jpg';
import cartImg4 from '../../../assets/img/cart/cart-img-4.jpg';

class CartList extends React.Component {
    render() {
        return (
            <section class="cart-area ptb-100">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-8 col-md-12">
                            <form>
                                <div class="cart-wraps">
                                    <div class="cart-table table-responsive">
                                        <table class="table table-bordered">
                                            <thead>
                                                <tr>
                                                    <th scope="col">تصویر</th>
                                                    <th scope="col">نام محصول</th>
                                                    <th scope="col">قیمت</th>
                                                    <th scope="col">تعداد</th>
                                                    <th scope="col">جمع</th>
                                                </tr>
                                            </thead>

                                            <Right
                                                src={cartImg1}
                                                product1="ماکت جلد کتاب"
                                                price="29ریال"

                                            />

                                            <Right
                                                src={cartImg2}
                                                product1="ماکت جلد کتاب"
                                                price="29ریال"
                                            />

                                            <Right
                                                src={cartImg3}
                                                product1="ماکت جلد کتاب"
                                                price="29ریال"

                                            />

                                            <Right
                                                src={cartImg4}
                                                product1="ماکت جلد کتاب"
                                                price="29ریال"

                                            />
                                        </table>
                                    </div>
                                </div>
                            </form>
                        </div>

                        <Left/>
                        
                    </div>
                </div>
            </section>
        )
    }
}


export default CartList;