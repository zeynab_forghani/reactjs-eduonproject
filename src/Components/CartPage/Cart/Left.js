import React from 'react';

class Left extends React.Component {
    render() {
        return (
            <div class="col-lg-4">
                <h3 class="cart-checkout-title">خلاصه پرداخت</h3>
                <div class="cart-totals">
                    <ul>
                        <li>جمع کل <span>ریال123</span></li>
                        <li>حمل دریایی <span>ریال00</span></li>
                        <li>جمع <span>ریال123</span></li>
                        <li><b>کل قابل پرداخت</b> <span><b>ریال123</b></span></li>
                    </ul>

                    <a href="checkout.html" class="default-btn two">
                    هم اکنون خریداری کنید
							</a>
                </div>
            </div>
        )
    }
}

export default Left;