import React from 'react';



class Right extends React.Component {
    render() {
        return (
            

                                <tbody>
                                    <tr>
                                        <td class="product-thumbnail">
                                            <a href="#">
                                                <img src={this.props.src} alt="Image" />
                                            </a>
                                        </td>

                                        <td class="product-name">
                                            <a href="#">{this.props.product1}</a>
                                        </td>

                                        <td class="product-price">
                                            <span class="unit-amount">{this.props.price}</span>
                                        </td>

                                        <td class="product-quantity">
                                            <div class="input-counter">
                                                <span class="minus-btn">
                                                    <i class='bx bx-minus' ></i>
                                                </span>
                                                <input type="text" value="1" />
                                                <span class="plus-btn">
                                                    <i class='bx bx-plus' ></i>
                                                </span>
                                            </div>
                                        </td>

                                        <td class="product-subtotal">
                                            <span class="subtotal-amount">{this.props.price}</span>

                                            <a href="#" class="remove">
                                                <i class="bx bx-x"></i>
                                            </a>
                                        </td>
                                    </tr>
                                </tbody>
                

        )
    }
}

export default Right;