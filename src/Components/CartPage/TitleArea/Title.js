import React from 'react';
import { Link } from 'react-router-dom';

class Title extends React.Component {

	render(){
		return(
<div class="page-title-area bg-20">
			<div class="container">
				<div class="page-title-content">
					<h2>صفحه سبد خرید</h2>
					<ul>
						<li>
							<Link to="/">
								خانه 
							</Link>
						</li>
						
						<li class="active">سبد خرید</li>
					</ul>
				</div>
			</div>
		</div>

			)
		}
	}

export default Title;