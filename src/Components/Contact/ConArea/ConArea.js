import React from 'react';

class ConArea extends React.Component {

	render(){
		return(

            <section class="main-contact-area pb-100">
			<div class="container">
				<div class="row">
					<div class="col-12">
						<div class="contact-wrap contact-pages mb-0">
							<div class="contact-form">
								<div class="section-title">
									<h2>برای هرگونه سوالی پیامی برای ما ارسال کنید</h2>
									<p>برای کسب اطلاعات بیشتر در مورد دوره های ما ، در تماس باشید <br />با مخاطبین eduon</p>
										
								</div>
								<form id="contactForm">
									<div class="row">
										<div class="col-lg-6 col-sm-6">
											<div class="form-group">
												<label>نام</label>
												<input type="text" name="name" id="name" class="form-control" required data-error="Please enter your name" />
												<div class="help-block with-errors"></div>
											</div>
										</div>
			
										<div class="col-lg-6 col-sm-6">
											<div class="form-group">
												<label>ایمیل</label>
												<input type="email" name="email" id="email" class="form-control" required data-error="Please enter your email" />
												<div class="help-block with-errors"></div>
											</div>
										</div>
			
										<div class="col-12">
											<div class="form-group">
												<label>موضوع</label>
												<input type="text" name="msg_subject" id="msg_subject" class="form-control" required data-error="Please enter your subject" />
												<div class="help-block with-errors"></div>
											</div>
										</div>
			
										<div class="col-12">
											<div class="form-group">
												<label>متن پیام</label>
												<textarea name="message" class="form-control" id="message" cols="30" rows="10" required data-error="Write your message"></textarea>
												<div class="help-block with-errors"></div>
											</div>
										</div>
			
										<div class="col-lg-12 col-md-12">
											<button type="submit" class="default-btn btn-two">
												ارسال پیام
											</button>
											<div id="msgSubmit" class="h3 text-center hidden"></div>
											<div class="clearfix"></div>
										</div>
									</div>
								</form>
							</div>
						</div>
					</div>
				</div>
			</div>
		</section>

			)
		}
	}

export default ConArea;