import React from 'react';
import Title from './TitleArea/Title';
import Info from './InfoArea/Info';
import ConArea from './ConArea/ConArea';
import Map from './MapArea/Map';
import Footer from '../Footer/Footer';

class Contact extends React.Component {

	render(){
		return(
            
            <div className="Contact">
                <Title/>
				<Info/>
				<ConArea/>
				<Map/>
				<Footer/>
            </div>


			)
		}
	}

export default Contact;