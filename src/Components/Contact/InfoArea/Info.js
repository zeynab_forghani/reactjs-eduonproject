import React from 'react';

class Info extends React.Component {

	render(){
		return(
<section class="contact-info-area pt-100 pb-70">
			<div class="container">
				<div class="row">
					<div class="col-lg-4 col-sm-6">
						<div class="single-contact-info">
							<i class="flaticon-call"></i>
							<h3>با ما تماس بگیرید</h3>
							<a href="tel:+1(514)312-5678">تلفن : 30459604069</a>
							<a href="tel:+1(514)312-6688">شماره همراه : 494495959764</a>
						</div>
					</div>

					<div class="col-lg-4 col-sm-6">
						<div class="single-contact-info">
							<i class="flaticon-pin"></i>
							<h3>مکان ما</h3>
							<a href="#"> نیویورک ، ایالات متحده آمریکا</a>
						</div>
					</div>

					<div class="col-lg-4 col-sm-6 offset-sm-3 offset-lg-0">
						<div class="single-contact-info">
							<i class="flaticon-email"></i>
							<h3>ایمیل</h3>
							<a href="mailto:hello@eduon.com">hello@eduon.com</a>
							<a href="mailto:public@eduon.com">public@eduon.com</a>
						</div>
					</div>
				</div>
			</div>
		</section>

			)
		}
	}

export default Info;