import React from 'react';

class Footer extends React.Component {
    render(){
        return(

            // start top area 
            <div className="Footer">

            <footer class="footer-top-area pt-100 pb-70">
			<div class="container">
				<div class="row">
					<div class="col-lg-3 col-sm-6">
						<div class="footer-widget">
							<h3>پیدا کردن ما</h3>

							<ul class="address">
								<li class="location">
									<i class="bx bxs-location-plus"></i>
									نیویورک ، ایالات متحده آمریکا
								</li>

								<li>
									<i class="bx bxs-envelope"></i>
									<a href="mailto:hello@eduon.com">hello@eduon.com</a>
									<a href="mailto:public@eduon.com">public@eduon.com</a>
								</li>

								<li>
									<i class="bx bxs-phone-call"></i>
									<a href="tel:+1(514)312-5678">+1 (514) 312-5678</a>
									<a href="tel:+1(514)312-6688">+1 (514) 312-6688</a>
								</li>
							</ul>
						</div>
					</div>

					<div class="col-lg-3 col-sm-6">
						<div class="footer-widget">
							<h3>لینک های مفید</h3>

							<ul class="link">
								<li>
									<a href="courses.html">همه دوره ها</a>
								</li>
								<li>
									<a href="about.html">درباره ما</a>
								</li>
								<li>
									<a href="faq.html">راهنما (سوالات متداول)</a>
								</li>
								<li>
									<a href="terms-conditions.html">شرایط و ضوابط</a>
								</li>
								<li>
									<a href="privacy-policy.html">سیاست حفظ حریم خصوصی</a>
								</li>
							</ul>
						</div>
					</div>

					<div class="col-lg-3 col-sm-6">
						<div class="footer-widget">
							<h3>دوره های برتر آنلاین</h3>

							<ul class="link">
								<li>
									<a href="#">هوش مصنوعی برای همه</a>
								</li>
								<li>
									<a href="#">شبکه ها و یادگیری عمیق</a>
								</li>
								<li>
									<a href="#">یادگیری با پایتون</a>
								</li>
								<li>
									<a href="#">بازارهای مالی</a>
								</li>
								<li>
									<a href="#">انگلیسی صحبت کنید حرفه ای</a>
								</li>
							</ul>
						</div>
					</div>

					<div class="col-lg-3 col-sm-6">
						<div class="footer-widget">
							<h3>سوژه های محبوب</h3>

							<ul class="link">
								<li>
									<a href="#">علم داده</a>
								</li>
								<li>
									<a href="#">علوم کامپیوتر</a>
								</li>
								<li>
									<a href="#">تجارت و مدیریت</a>
								</li>
								<li>
									<a href="#">تجارت و مدیریت</a>
								</li>
								<li>
									<a href="#">طبیعت و محیط زیست</a>
								</li>
							</ul>
						</div>
					</div>
				</div>
			</div>
		</footer>

        {/* start bottom area  */}

        <footer class="footer-bottom-area">
			<div class="container">
				<div class="copyright-wrap">
					<p>حق چاپ @ 2020 طراحی شده توسط <a href="https://hibootstrap.com/" target="blank">HiBootstrap.com</a></p>
				</div>
			</div>
		</footer>
        </div>

        )
    }
}


export default Footer;
