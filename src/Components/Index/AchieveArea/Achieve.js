import React from 'react';
import AchieveList from './AchieveList';
import shapeImg1 from '../../../assets/img/achieve-shape/achieve-shape-1.png';
import shapeImg2 from '../../../assets/img/achieve-shape/achieve-shape-2.png';
import shapeImg3 from '../../../assets/img/achieve-shape/achieve-shape-3.png';
import shapeImg4 from '../../../assets/img/achieve-shape/achieve-shape-4.png';


class Achieve extends React.Component {
    render() {
        return (
            <div className="Archive">

                <section className="achieve-area f5f6fa-bg-color pt-100 pb-70">
                    <div className="container">
                        <div className="section-title">
                            <span>رهبر در آموزش و پرورش</span>
                            <h2>به اهداف خود برسید</h2>
                        </div>
                        <div className="row">
                            <AchieveList
                                title="جدیدترین مهارت ها را بیاموزید"
                                text="لورم ایپسوم متن ساختگی با تولید سادگی نامفهوم از صنعت چاپ و با استفاده از طراحان گرافیک است چاپگرها و متون بلکه روزنامه و مجله در ستون و سطرآنچنان که لازم است"
                                src={shapeImg1}
                                icon="flaticon-skills"
                            />

                            <AchieveList
                                title="برای یک حرفه آماده شوید"
                                text="لورم ایپسوم متن ساختگی با تولید سادگی نامفهوم از صنعت چاپ و با استفاده از طراحان گرافیک است چاپگرها و متون بلکه روزنامه و مجله در ستون و سطرآنچنان که لازم است"
                                src={shapeImg2}
                                icon="flaticon-career"
                            />

                            <AchieveList
                                title="گواهی یا مدرک کسب کنید "                               
                                text="لورم ایپسوم متن ساختگی با تولید سادگی نامفهوم از صنعت چاپ و با استفاده از طراحان گرافیک است چاپگرها و متون بلکه روزنامه و مجله در ستون و سطرآنچنان که لازم است"
                                src={shapeImg3}
                                icon="flaticon-certificate"
                            />

                            <AchieveList
                                title="جدیدترین مهارت ها را بیاموزید"
                                text="لورم ایپسوم متن ساختگی با تولید سادگی نامفهوم از صنعت چاپ و با استفاده از طراحان گرافیک است چاپگرها و متون بلکه روزنامه و مجله در ستون و سطرآنچنان که لازم است"
                                src={shapeImg4}
                                icon="flaticon-group"
                            />

                        </div>

                    </div>
                </section>

            </div>
        )
    }
}


export default Achieve;