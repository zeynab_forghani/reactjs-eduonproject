import React from 'react';


class AchieveList extends React.Component {
  render() {
    const {title , text , src , icon} = this.props;
    return (
      <div className="col-lg-3 col-sm-6" >
        <div class="single-achieve">
          <div class="achieve-shape shape-1">
            <img src={src} />
            <i className={icon}></i>
          </div>

          <h3>{title}</h3>
          <p>{text}</p>
        </div>
      </div>

    )
  }
}




export default AchieveList;