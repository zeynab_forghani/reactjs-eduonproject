import React from 'react';
import BannerList from './BannerList';

class Banner extends React.Component {
    state = {
        items:[
            {    
            title:"با متخصصان یادگیری آنلاین یک صلاحیت شناخته شده را مطالعه کنید" ,
            text:"لورم ایپسوم متن ساختگی با تولید سادگی نامفهوم از صنعت چاپ و با استفاده از طراحان گرافیک است چاپگرها و متون بلکه روزنامه و مجله در ستون" ,
            buttonText:"مشاهده دوره ها",
            socialWrap:"ما را دنبال کنید:"
            
        }
        
        ]
    }
    
    render(){
        return(

            <div className="Banner">
                <BannerList items={this.state.items} />
            </div>

        )
    }
}

export default Banner;