import React from 'react';
import { Link } from 'react-router-dom';
import bannerImage from "../../../assets/img/banner-img/banner-img-1.jpg";


class BannerList extends React.Component {
    render (){
       const {items} = this.props;
       const bannerList = items.map(item=>{
           return(
            <div classNameName="Banner">


		<section className="banner-area f5f6fa-bg-color">
			<div className="container-fluid social">
				<div className="row align-items-center">
					<div className="col-lg-6">
						<div className="banner-content">
           <h1 className="wow fadeInLeft" data-wow-delay="0.3s">{item.title}</h1>
           <p className="wow fadeInLeft" data-wow-delay="0.6s">{item.text}</p>

							<Link to="courses.html" className="default-btn wow fadeInLeft" data-wow-delay="0.9s"> 
								{item.buttonText}
							</Link>
						</div>
					</div>

					<div className="col-lg-6">
						<div className="banner-img wow fadeInRight" data-wow-delay="0.3s">
							<img src={bannerImage} alt="Image" />
						</div>
					</div>
				</div>

				
				<ul className="social-wrap">
					<li className="follow-us">
						{item.socialWrap}
					</li>
					<li>
						<a href="#" target="_blank">
							<i className="bx bxl-twitter"></i>
						</a>
					</li>
					<li>
						<a href="#" target="_blank">
							<i className="bx bxl-instagram"></i>
						</a>
					</li>
					<li>
						<a href="#" target="_blank">
							<i className="bx bxl-facebook"></i>
						</a>
					</li>
					<li>
						<a href="#" target="_blank">
							<i className="bx bxl-linkedin"></i>
						</a>
					</li>
				</ul>
				
			</div>
		</section>
		

            </div>
           )
       })  


        return (
         <div>
            {bannerList}
         </div>
        )
    }
}

export default BannerList;