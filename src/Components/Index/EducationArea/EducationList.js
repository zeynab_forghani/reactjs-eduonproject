import React from 'react';
import { Link } from 'react-router-dom';
import educationImg from '../../../assets/img/education-img.png';

class Education extends React.Component {
	render() {

		const { items } = this.props;
		const educationList = items.map(item => {
			return (

				<section class="education-area ebeef5-bg-color">
					<div class="container-fluid p-0">
						<div class="row">
							<div class="col-lg-6">
								<div class="education-img">
									<img src={educationImg} alt="Image" />
								</div>
							</div>

							<div class="col-lg-6">
								<div class="education-content ptb-100">
									<span class="top-title">{item.toptitle}</span>
									<h2>{item.title1}<span>{item.title2}</span>{item.title3}</h2>
									<p>{item.paragraph1}</p>
									<p>{item.paragraph2}</p>

									<ul>
										<li>
											<i class="bx bx-check"></i>
											{item.li1}
										</li>
										<li>
											<i class="bx bx-check"></i>
											{item.li2}
										</li>
										<li>
											<i class="bx bx-check"></i>
											{item.li3}
										</li>
										<li>
											<i class="bx bx-check"></i>
											{item.li4}
										</li>
										<li>
											<i class="bx bx-check"></i>
											{item.li5}
										</li>
									</ul>

									<Link to="courses.html" class="default-btn">
										{item.buttonText}
							</Link>
								</div>
							</div>
						</div>
					</div>
				</section>

			)
		})



		return (

			<div className="EducationList">
				{educationList}
			</div>

		)
	}
}


export default Education;