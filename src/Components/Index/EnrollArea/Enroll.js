import React from 'react';
import Form from './Form';
import LeftPic from './LeftPic';
import enrollImg from '../../../assets/img/enroll-img.png';

class Enroll extends React.Component {
    render() {
        return (
            <section class="enroll-area ptb-100">
                <div class="container">
                    <div class="row">
                        <LeftPic
                            src={enrollImg}
                        />
                    </div>
                    <div class="row">
                    <Form
                        span="به هر دوره ای نیاز دارید"
                        h3="اکنون ثبت نام کنید"
                        inputtext="اسم شما"
                        inputemail="ایمیل شما"
                        inputnum="شماره همراه"
                        button="ثبت نام"
                        coursestype="عنوان دوره"
                        coursestype1="عنوان دوره1"
                        coursestype2="عنوان دوره2"
                        coursestype3="عنوان دوره3"
                        coursestype4="عنوان دوره4"

                    />
</div>
                </div>
            </section>
        )
    }
}


export default Enroll;