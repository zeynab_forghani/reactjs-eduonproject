import React from 'react';

class Form extends React.Component {
    render() {
        return (
            <div class="col-lg-5">
                <div class="enroll-wrap">
                    <form class="courses-form">
                        <span>{this.props.span}</span>
                        <h3>{this.props.h3}</h3>

                        <div class="form-group">
                            <input type="text" class="form-control" id="Name" placeholder={this.props.inputtext} />
                        </div>

                        <div class="form-group">
                            <input type="email" class="form-control" id="email" placeholder={this.props.inputemail} />
                        </div>

                        <div class="form-group">
                            <input type="text" class="form-control" id="Number" placeholder={this.props.inputnum} />
                        </div>

                        <div class="form-group">
                            <select>
                                <option value="">{this.props.coursestype}</option>
                                <option value="">{this.props.coursestype1}</option>
                                <option value="">{this.props.coursestype2}</option>
                                <option value="">{this.props.coursestype3}</option>
                                <option value="">{this.props.coursestype4}</option>

                            </select>
                        </div>

                        <button type="submit" class="default-btn">
                            {this.props.button}
                        </button>
                    </form>
                </div>
            </div>

        )
    }
}

export default Form;