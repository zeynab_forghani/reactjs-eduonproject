import React from 'react';

class LeftPic extends React.Component {
    render (){
        
        return(
            <div class="col-lg-7">
						<div class="enroll-img">
							<img src={this.props.enrollImg} alt="Image" />
						</div>
					</div>
        )
    }
}

export default LeftPic;