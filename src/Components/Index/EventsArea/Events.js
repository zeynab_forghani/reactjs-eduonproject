import React from 'react';
import EventsList from './EventsList';
import eventsImg1 from '../../../assets/img/event-img/event-img-1.png';
import eventsImg2 from '../../../assets/img/event-img/event-img-2.png';
import eventsImg3 from '../../../assets/img/event-img/event-img-3.png';
import eventsImg4 from '../../../assets/img/event-img/event-img-4.png';

class Events extends React.Component {
    render() {
        return (
            <section class="event-area ptb-70">
                <div class="container">
                    <div class="section-title">
                        <span>رویدادهای آموزشی</span>
                        <h2>رویدادهای آینده</h2>
                    </div>
                    <div class="row">

                        <EventsList
                            src={eventsImg1}
                            data="13 آگوست 2020"
                            time="دوشنبه 3:00 صبح - 5:00 عصر"
                            title="همایش جامع سواد آموزی و بازیابی"
                            place="ایالت واشنگتن"
                        />

                        <EventsList
                            src={eventsImg2}
                            data="13 آگوست 2020"
                            time="دوشنبه 3:00 صبح - 5:00 عصر"
                            title="همایش جامع سواد آموزی و بازیابی"
                            place="ایالت واشنگتن"
                        />

                        <EventsList
                            src={eventsImg3}
                            data="13 آگوست 2020"
                            time="دوشنبه 3:00 صبح - 5:00 عصر"
                            title="همایش جامع سواد آموزی و بازیابی"
                            place="ایالت واشنگتن"
                        />

                        <EventsList
                            src={eventsImg4}
                            data="13 آگوست 2020"
                            time="دوشنبه 3:00 صبح - 5:00 عصر"
                            title="همایش جامع سواد آموزی و بازیابی"
                            place="ایالت واشنگتن"
                        />
                    </div>
                </div>
            </section>
        )
    }
}

export default Events;