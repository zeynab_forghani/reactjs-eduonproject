import React from 'react';

class Events extends React.Component {
    render(){
        return(

					<div class="col-lg-6 col-sm-6">
						<div class="single-event">
							<a href="single-event.html">
								<img src={this.props.src} alt="Image" />
							</a>

							<div class="event-content">
								<ul>
									<li>
										<i class="bx bx-calendar"></i>
										{this.props.data}
									</li>
									<li>
										<i class="bx bx-time"></i>
										{this.props.time}
									</li>
								</ul>

								<a href="single-event.html">
        <h3>{this.props.title}</h3>
								</a>
								
								<span>
									<i class="bx bxs-location-plus"></i> 
									{this.props.place}
								</span>
							</div>
						</div>
					</div>
        )
    }
}


export default Events;