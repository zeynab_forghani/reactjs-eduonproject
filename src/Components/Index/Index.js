import React from 'react';
import Banner from './BannerArea/Banner';
import Achieve from './AchieveArea/Achieve';
import Education from './EducationArea/Education';
import Popular from './PopularCourses/Popular';
import Enroll from './EnrollArea/Enroll';
import Teacher from './TeachersArea/Teachers';
import Events from './EventsArea/Events';
import Video from './VideoArea/Video';
import News from './NewsArea/News';
import Subscribe from './SubscribeArea/Subscribe';
import Footer from '../Footer/Footer';

class Index extends React.Component {
    render(){
        return(
            <div className="Index">
                <Banner/>
                <Achieve/>
                <Education/>
                <Popular/>
                <Enroll/>
                <Teacher/>
                <Events/>
                <Video/>
                <News/>
                <Subscribe/>
                <Footer/>
            </div>
        )
    }
}

export default Index;