import React from 'react';

class Left extends React.Component {
	render() {
		return (

			<div class="col-lg-6">
				<div class="single-news">
					<a href="single-blog.html">
						<img src={this.props.src} alt="Image" />
					</a>

					<div class="news-content">
						<span class="tag">{this.props.tag}</span>

						<a href="single-blog.html">
							<h3>{this.props.single}</h3>
						</a>

						<ul class="lessons">
							<li>{this.props.li1}<a href="#">{this.props.li2}</a></li>
							<li class="float">{this.props.float}</li>
						</ul>
					</div>
				</div>
			</div>
		)
	}
}


export default Left;