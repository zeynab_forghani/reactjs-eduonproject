import React from 'react';
import Left from './Left';
import Right from './Right';
import newsImg from '../../../assets/img/news-img/news-img-1.jpg';


class News extends React.Component {
    render() {
        return (

            <section class="news-area f5f6fa-bg-color pt-100 pb-70">
                <div class="container">
                    <div class="section-title">
                        <span>اخبار ما</span>
                        <h2>اخبار اخیر را کاوش کنید</h2>
                    </div>

                    <div class="row">
                        <Left
                            src={newsImg}
                            tag="توسعه"
                            single="یادگیری مادام العمر چیست و چگونه می تواند به یادگیری شما کمک کند؟ "
                            li1="توسط:"
                            li2="مدیر"
                            float="ارسال شده در تاریخ 20/07/2020 "
                        />
                        <div class="col-lg-6">
                            <Right
                                tag="پژوهش"
                                single="تعیین هدف واقعی یک آموزش خوب دشوار است"
                                li1="توسط:"
                                li2="مدیر"
                                float="ارسال شده در تاریخ 20/07/2020"
                            />

                            <Right
                                tag="پژوهش"
                                single="تعیین هدف واقعی یک آموزش خوب دشوار است"
                                li1="توسط:"
                                li2="مدیر"
                                float="ارسال شده در تاریخ 20/07/2020"
                            />
                            <Right
                                tag="پژوهش"
                                single="تعیین هدف واقعی یک آموزش خوب دشوار است"
                                li1="توسط:"
                                li2="مدیر"
                                float="ارسال شده در تاریخ 20/07/2020"
                            />

                        </div>
                    </div>
                </div>
            </section>
        )
    }
}

export default News;