import React from 'react';

class Right extends React.Component {
    render() {
        return (

            <div className="Right">
                <div class="row mb-30">
                    <div class="col-lg-4 col-sm-4 pr-0">
                        <div class="news-img bg-3">
                        </div>
                    </div>
                    <div class="col-lg-8 col-sm-8 pl-0">
                        <div class="news-listing-content">
                            <span class="tag">{this.props.tag}</span>

                            <a href="single-blog.html">
                                <h3>{this.props.single}</h3>
                            </a>

                            <ul class="lessons">
                                <li>{this.props.li1}<a href="#">{this.props.li2}</a></li>
                                <li class="float">{this.props.float}</li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>

        )
    }
}

export default Right;