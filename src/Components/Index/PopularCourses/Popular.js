import React from 'react';
import PopularList from './PopularList';

import courseImg1 from '../../../assets/img/course-img/course-img-1.jpg';
import courseImg2 from '../../../assets/img/course-img/course-img-2.jpg';
import courseImg3 from '../../../assets/img/course-img/course-img-3.jpg';
import courseImg4 from '../../../assets/img/course-img/course-img-4.jpg';
import courseImg5 from '../../../assets/img/course-img/course-img-5.jpg';
import courseImg6 from '../../../assets/img/course-img/course-img-6.jpg';


class Popular extends React.Component {
    render() {

        return (

            <section class="courses-area pt-100 pb-70">
                <div class="container">
                    <div class="section-title">
                        <span>دوره های محبوب</span>
                        <h2>برای کسب اطلاعات بیشتر یک دوره را انتخاب کنید</h2>
                    </div>
                    <div class="row">
                        <PopularList
                            src={courseImg1}
                            price="39ريال"
                            tag="آموزش"
                            title="تدوین استراتژی برای آموزش و یادگیری آنلاین"
                            span="0.5"
                            rating="(1 امتیاز)"
                            paragraph="لورم ایپسوم متن ساختگی با تولید سادگی نامفهوم از صنعت چاپ و با استفاده از طراحان گرافیک است"
                            lesson="0 درس"
                            student="44 دانش آموز"
                        />

                        <PopularList
                            src={courseImg2}
                            price="39ريال"
                            tag="آموزش"
                            title="تدوین استراتژی برای آموزش و یادگیری آنلاین"
                            span="0.5"
                            rating="(1 امتیاز)"
                            paragraph="لورم ایپسوم متن ساختگی با تولید سادگی نامفهوم از صنعت چاپ و با استفاده از طراحان گرافیک است"
                            lesson="0 درس"
                            student="44 دانش آموز"
                        />

                        <PopularList
                            src={courseImg3}
                            price="39ريال"
                            tag="آموزش"
                            title="تدوین استراتژی برای آموزش و یادگیری آنلاین"
                            span="0.5"
                            rating="(1 امتیاز)"
                            paragraph="لورم ایپسوم متن ساختگی با تولید سادگی نامفهوم از صنعت چاپ و با استفاده از طراحان گرافیک است"
                            lesson="0 درس"
                            student="44 دانش آموز"
                        />

                        <PopularList
                            src={courseImg4}
                            price="39ريال"
                            tag="آموزش"
                            title="تدوین استراتژی برای آموزش و یادگیری آنلاین"
                            span="0.5"
                            rating="(1 امتیاز)"
                            paragraph="لورم ایپسوم متن ساختگی با تولید سادگی نامفهوم از صنعت چاپ و با استفاده از طراحان گرافیک است"
                            lesson="0 درس"
                            student="44 دانش آموز"
                        />

                        <PopularList
                            src={courseImg5}
                            price="39ريال"
                            tag="آموزش"
                            title="تدوین استراتژی برای آموزش و یادگیری آنلاین"
                            span="0.5"
                            rating="(1 امتیاز)"
                            paragraph="لورم ایپسوم متن ساختگی با تولید سادگی نامفهوم از صنعت چاپ و با استفاده از طراحان گرافیک است"
                            lesson="0 درس"
                            student="44 دانش آموز"
                        />

                        <PopularList
                            src={courseImg6}
                            price="39ريال"
                            tag="آموزش"
                            title="تدوین استراتژی برای آموزش و یادگیری آنلاین"
                            span="0.5"
                            rating="(1 امتیاز)"
                            paragraph="لورم ایپسوم متن ساختگی با تولید سادگی نامفهوم از صنعت چاپ و با استفاده از طراحان گرافیک است"
                            lesson="0 درس"
                            student="44 دانش آموز"
                        />

                   
                    </div>
                </div>
            </section>

        )
    }
}

export default Popular;