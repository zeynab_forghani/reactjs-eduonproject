import React from 'react';

class PopularList extends React.Component {
    render() {
        return (
            <div class="col-lg-4 col-md-6">
                <div class="single-course">
                    <a href="single-course.html">
                        <img src={this.props.src} alt="Image" />
                    </a>

                    <div class="course-content">
                        <span class="price">{this.props.price}</span>
                        <span class="tag">{this.props.tag}</span>

                        <a href="single-course.html">
                            <h3>{this.props.title}</h3>
                        </a>

                        <ul class="rating">
                            <li>
                                <i class="bx bxs-star"></i>
                            </li>
                            <li>
                                <i class="bx bxs-star"></i>
                            </li>
                            <li>
                                <i class="bx bxs-star"></i>
                            </li>
                            <li>
                                <i class="bx bxs-star"></i>
                            </li>
                            <li>
                                <i class="bx bxs-star"></i>
                            </li>
                            <li>
                                <span>{this.props.span}</span>
        <a href="#">{this.props.rating}</a>
                            </li>
                        </ul>

        <p>{this.props.paragraph}</p>

                        <ul class="lessons">
        <li>{this.props.lesson}</li>
        <li class="float">{this.props.student}</li>
                        </ul>
                    </div>
                </div>
            </div>

        )
    }
}


export default PopularList;