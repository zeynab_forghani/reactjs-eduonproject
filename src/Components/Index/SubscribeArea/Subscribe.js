import React from 'react';
import subscribeImg from '../../../assets/img/subscribe-img.png';

class Subscribe extends React.Component {
    render(){
        return(

            <section class="subscribe-area ebeef5-bg-color ptb-100">
			<div class="container">
				<div class="subscribe-wrap">
					<h2>اشتراک</h2>
					<p>در خبرنامه ما مشترک شوید و به روز باشید</p>

					<form class="newsletter-form" data-toggle="validator">
						<input type="email" class="form-control" placeholder="ایمیل شما" name="EMAIL" required autocomplete="off" />

						<button class="default-btn" type="submit">مشترک شوید</button>

						<div id="validator-newsletter" class="form-result"></div>
					</form>	
					<div class="subscribe-img">
						<img src={subscribeImg} alt="Image" />
					</div>
				</div>
			</div>
		</section>

        )
    }
}


export default Subscribe;