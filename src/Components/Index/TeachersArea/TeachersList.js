import React from 'react';

class TeacherList extends React.Component {
    render() {
        return (

            <div class="col-lg-3 col-sm-6">
                <div class="single-teachers">
                    <img src={this.props.src} alt="Image" />

                    <div class="teachers-content">
                        <ul>
                            <li>
                                <a href="#"><i class="bx bxl-instagram"></i></a>
                            </li>
                            <li>
                                <a href="#"><i class="bx bxl-facebook"></i></a>
                            </li>
                            <li>
                                <a href="#"><i class="bx bxl-linkedin-square"></i></a>
                            </li>
                        </ul>

                        <h3>{this.props.name}</h3>
                        <span>{this.props.job}</span>
                    </div>
                </div>
            </div>
        )
    }
}


export default TeacherList;