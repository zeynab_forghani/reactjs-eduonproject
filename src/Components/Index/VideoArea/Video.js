import React from 'react';
import videoImg from '../../../assets/img/video-img.jpg';
class Video extends React.Component {
    render(){
        return(

            <div class="video-area f5f6fa-bg-color">
			<div class="container">
				<div class="video-wrap">
					<img src={videoImg} alt="Image" />
					
					<div class="video-content">
						<a href="https://www.youtube.com/watch?v=iLS_YP1uEK8" class="video-btn popup-youtube">
							<i class="flaticon-play-button"></i>
						</a>
					</div>
				</div>
			</div>
		</div>
		

        )
    }
}


export default Video;