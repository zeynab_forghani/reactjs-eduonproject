import React from 'react';
import logoImage from '../../assets/img/logo.png';
import {Link} from "react-router-dom";




class Navbar extends React.Component {
    render() {
        return (

            <div className="navbar-area">

                <div className="mobile-nav">
                    <a href="index.html" className="logo">
                        <img src={logoImage} alt="Logo" />
                    </a>
                </div>


                <div className="main-nav">
                    <div className="container-fluid">
                        <nav className="navbar navbar-expand-md">
                            <a className="navbar-brand" href="index.html">
                                <img src={logoImage} alt="Logo" />
                            </a>

                            <div className="collapse navbar-collapse mean-menu">
                                <ul className="navbar-nav m-auto">
                                <li className="nav-item">
                                        <Link to="/" className="nav-link">
                                            خانه
                                        </Link>
                                    </li>

                                    <li className="nav-item">
                                        <Link to="/About" className="nav-link">
                                            درباره ما
                                    
                                        </Link>
                                    </li>


                                        <li className="nav-item">
                                            <Link to="/Shop" className="nav-link">
                                                خرید
                                    <i className="bx bx-chevron-down"></i>
                                            </Link>

                                            <ul className="dropdown-menu">
                                                <li className="nav-item">
                                                    <Link to="/shop" className="nav-link">خرید</Link>
                                                </li>
                                                <li className="nav-item">
                                                    <Link to="/cart" className="nav-link">سبد خرید</Link>
                                                </li>
                                            </ul>
                                        </li>

                                       
                                        <li className="nav-item">
                                            <Link to="/contact" className="nav-link">ارتباط با ما</Link>
                                        </li>
                        </ul>


                                    <div className="others-option">
                                        <div className="option-item">
                                            <i className="search-btn bx bx-search"></i>
                                            <i className="close-btn bx bx-x"></i>

                                            <div className="search-overlay search-popup">
                                                <div className='search-box'>
                                                    <form className="search-form">
                                                        <input className="search-input" name="search" placeholder="Search" type="text" />

                                                        <button className="search-button" type="submit"><i className="bx bx-search"></i></button>
                                                    </form>
                                                </div>
                                            </div>
                                        </div>

                                        <div className="cart-icon">
                                            <Link to="/cart">
                                                <i className="flaticon-shopping-cart"></i>
                                                <span>0</span>
                                            </Link>
                                        </div>

                                        <div className="register">
                                            <Link to="my-account.html" className="default-btn">
                                              ورود / ثبت نام
                                </Link>
                                        </div>
                                    </div>
                        
                    </div>
                </nav>
            </div>
                    </div>


                    <div className="others-option-for-responsive">
                        <div className="container">
                            <div className="dot-menu">
                                <div className="inner">
                                    <div className="circle circle-one"></div>
                                    <div className="circle circle-two"></div>
                                    <div className="circle circle-three"></div>
                                </div>
                            </div>

                            <div className="container">
                                <div className="option-inner">
                                    <div className="others-option justify-content-center d-flex align-items-center">
                                        <div className="option-item">
                                            <i className="search-btn bx bx-search"></i>
                                            <i className="close-btn bx bx-x"></i>

                                            <div className="search-overlay search-popup">
                                                <div className='search-box'>
                                                    <form className="search-form">
                                                        <input className="search-input" name="search" placeholder="Search" type="text" />

                                                        <button className="search-button" type="submit"><i className="bx bx-search"></i></button>
                                                    </form>
                                                </div>
                                            </div>
                                        </div>

                                        <div className="cart-icon">
                                            <a href="cart.html">
                                                <i className="flaticon-shopping-cart"></i>
                                                <span>0</span>
                                            </a>
                                        </div>

                                        <div className="register">
                                            <a href="my-account.html" className="default-btn">
                                            ورود / ثبت نام
                                </a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
    
        )
    }
}

export default Navbar;