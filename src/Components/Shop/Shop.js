import React from 'react';
import Title from './TitleArea/Title';
import Shoppage from './ShopArea/Shoppage';
import Footer from '../Footer/Footer';
class Shop extends React.Component {
    render() {
        return (
            <div className="Shop">
                <Title />
                <Shoppage />
                <Footer/>
            </div>
        )
    }
}

export default Shop;
