import React from 'react';
import Right from '../../Index/NewsArea/Right';

class Left extends React.Component {
    render(){
        return(
            <div class="col-lg-4">
						<div class="widget-sidebar">
							<div class="sidebar-widget search">
								<form class="search-form">
									<input class="form-control" name="search" placeholder="Search here" type="text" />
									<button class="search-button" type="submit">
										<i class="bx bx-search"></i>
									</button>
								</form>	
							</div>
	
							<div class="sidebar-widget categories">
								<h3>Categories</h3>
	
								<ul>
									<li>
										<a href="#">Education</a>
									</li>
									<li>
										<a href="#">Health Coaching</a>
									</li>
									<li>
										<a href="#">Learning</a>
									</li>
									<li>
										<a href="#">Online</a>
									</li>
									<li>
										<a href="#">Academics</a>
									</li>
									<li>
										<a href="#">Admission</a>
									</li>
									<li>
										<a href="#">Student</a>
									</li>
									<li>
										<a href="#">Graduation</a>
									</li>
								</ul>
							</div>
	
							<div class="sidebar-widget popular-post">
								<h3 class="widget-title">Popular Posts</h3>
	
								<div class="post-wrap">
									<div class="item">
										<a href="single-blog.html" class="thumb">
											<span class="fullimage cover bg1" role="img"></span>
										</a>
	
										<div class="info">
											<h4 class="title">
												<a href="single-blog.html">We will begin to explore the next two levels</a>
	
												<span class="date">20-07-2020</span>
											</h4>
										</div>
									</div>
	
									<div class="item">
										<a href="single-blog.html" class="thumb">
											<span class="fullimage cover bg2" role="img"></span>
										</a>
	
										<div class="info">
											<h4 class="title">
												<a href="single-blog.html">Determining the true goal of a good education is difficult.</a>
													
												<span class="date">19-07-2020</span>
											</h4>
										</div>
									</div>
	
									<div class="item">
										<a href="single-blog.html" class="thumb">
											<span class="fullimage cover bg3" role="img"></span>
										</a>
	
										<div class="info">
											<h4 class="title">
												<a href="single-blog.html">Implementing peer assessment in online group</a> 
													
												<span class="date">18-07-2020</span>
											</h4>
										</div>
									</div>
								</div>
							</div>
	
							<div class="sidebar-widget tags">
								<h3>Tags</h3>
	
								<ul>
									<li>
										<a href="#">Education</a>
									</li>
									<li>
										<a href="#">College</a>
									</li>
									<li>
										<a href="#">College</a>
									</li>
									<li>
										<a href="#">Math</a>
									</li>
									<li>
										<a href="#">Design</a>
									</li>
									<li>
										<a href="#">Learning</a>
									</li>
									<li>
										<a href="#">Learning</a>
									</li>
								</ul>
							</div>
						</div>
					</div>
                    
        )
    }
}

export default Left;