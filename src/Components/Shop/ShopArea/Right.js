import React from 'react';
import { Link } from 'react-router-dom';


class Right extends React.Component {
    render() {
        return (

            <div class="col-lg-4 col-sm-6">
                <div class="single-shop">
                    <div class="shop-img">
                        <img src={this.props.src} alt="Image" />

                        <ul>
                            <li>
                                <a href="#">
                                    <a href="#product-view-one" data-toggle="modal">
                                        <i class="bx bx-show-alt"></i>
                                    </a>
                                </a>
                            </li>
                            <li>
                                <a href="#">
                                    <i class="bx bx-heart"></i>
                                </a>
                            </li>
                        </ul>
                    </div>

                    <h3>{this.props.h3}</h3>
                    <span> <del>{this.props.price1}</del>{this.props.price2}</span>

                    <Link to="/Cart" class="default-btn">
                        {this.props.add}
                    </Link>
                </div>
            </div>


        )
    }
}


export default Right;