import React from 'react';
import Right from './Right';
import Left from './Left';
import shopImg1 from '../../../assets/img/shop/shop-img-1.jpg';
import shopImg2 from '../../../assets/img/shop/shop-img-2.jpg';
import shopImg3 from '../../../assets/img/shop/shop-img-3.jpg';
import shopImg4 from '../../../assets/img/shop/shop-img-4.jpg';
import shopImg5 from '../../../assets/img/shop/shop-img-5.jpg';
import shopImg6 from '../../../assets/img/shop/shop-img-6.jpg';
import shopImg7 from '../../../assets/img/shop/shop-img-7.jpg';
import shopImg8 from '../../../assets/img/shop/shop-img-8.jpg';
import shopImg9 from '../../../assets/img/shop/shop-img-9.jpg';

class Shoppage extends React.Component {
    render() {
        return (
            <div class="shop-area ptb-100">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-8">
                            <div class="shop-card-wrap">
                                <div class="showing-result">
                                    <div class="row align-items-center">
                                        <div class="col-lg-6 col-sm-6">
                                            <div class="showing-result-count">
                                                <p>نمایش 1-8 از 14 نتیجه</p>
                                            </div>
                                        </div>

                                        <div class="col-lg-6 col-sm-6">
                                            <div class="showing-top-bar-ordering">
                                                <select>
                                                    <option value="1">مرتب سازی پیش فرض</option>
                                                    <option value="2">تحصیلات</option>
                                                    <option value="0">حسابداری</option>
                                                    <option value="3">زبان</option>
                                                    <option value="4">درس دادن</option>
                                                    <option value="5">پژوهش</option>
                                                    <option value="5">ارزیابی</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <Right
                                        src={shopImg1}
                                        h3="تبدیل: مقیاس Y (1)"
                                        price1="49ریال"
                                        price2="39ریال"
                                        add="افزودن به سبد خرید"
                                    />
                                    <Right
                                        src={shopImg2}
                                        h3="تبدیل: مقیاس Y (1)"
                                        price1="49ریال"
                                        price2="39ریال"
                                        add="افزودن به سبد خرید"
                                    />
                                    <Right
                                        src={shopImg3}
                                        h3="تبدیل: مقیاس Y (1)"
                                        price1="49ریال"
                                        price2="39ریال"
                                        add="افزودن به سبد خرید"
                                    />
                                    <Right
                                        src={shopImg4}
                                        h3="تبدیل: مقیاس Y (1)"
                                        price1="49ریال"
                                        price2="39ریال"
                                        add="افزودن به سبد خرید"
                                    />
                                    <Right
                                        src={shopImg5}
                                        h3="تبدیل: مقیاس Y (1)"
                                        price1="49ریال"
                                        price2="39ریال"
                                        add="افزودن به سبد خرید"
                                    />
                                    <Right
                                        src={shopImg6}
                                        h3="تبدیل: مقیاس Y (1)"
                                        price1="49ریال"
                                        price2="39ریال"
                                        add="افزودن به سبد خرید"
                                    />
                                    <Right
                                        src={shopImg7}
                                        h3="تبدیل: مقیاس Y (1)"
                                        price1="49ریال"
                                        price2="39ریال"
                                        add="افزودن به سبد خرید"
                                    />
                                    <Right
                                        src={shopImg8}
                                        h3="تبدیل: مقیاس Y (1)"
                                        price1="49ریال"
                                        price2="39ریال"
                                        add="افزودن به سبد خرید"
                                    />
                                    <Right
                                        src={shopImg9}
                                        h3="تبدیل: مقیاس Y (1)"
                                        price1="49ریال"
                                        price2="39ریال"
                                        add="افزودن به سبد خرید"
                                    />
                                </div>
                            </div>
                        </div>
                        <Left/>
                        <div class="col-lg-12 col-md-12">
									<div class="pagination-area">
										<a href="#" class="prev page-numbers">
											<i class="bx bx-chevron-left"></i>
										</a>
										<span class="page-numbers current" aria-current="page">1</span>
										<a href="#" class="page-numbers">2</a>
										<a href="#" class="page-numbers">3</a>
										<a href="#" class="page-numbers">4</a>
										
										<a href="#" class="next page-numbers">
											<i class="bx bx-chevron-right"></i>
										</a>
									</div>
								</div>
                    </div>
                </div>
            </div>


        )
    }
}

export default Shoppage;