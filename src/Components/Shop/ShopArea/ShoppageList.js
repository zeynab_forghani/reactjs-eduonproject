import React from 'react';




class ShopArea extends React.Component {

	render(){
		return(
<div class="shop-area ptb-100">
			<div class="container">
				<div class="row">
					<div class="col-lg-8">
						<div class="shop-card-wrap">
							<div class="showing-result">
								<div class="row align-items-center">
									<div class="col-lg-6 col-sm-6">
										<div class="showing-result-count">
											<p>Showing 1-8 of 14 results</p>
										</div>
									</div>
			
									<div class="col-lg-6 col-sm-6">
										<div class="showing-top-bar-ordering">
											<select>
												<option value="1">Default sorting</option>
												<option value="2">Education</option>
												<option value="0">Accounting</option>
												<option value="3">Language</option>
												<option value="4">Teaching</option>
												<option value="5">Research</option>
												<option value="5">Assessment</option>
											</select>
										</div>
									</div>
								</div>
							</div>
							
							<div class="row">
								<div class="col-lg-4 col-sm-6">
									<div class="single-shop">
										<div class="shop-img">
											<img src={shopImg1} alt="Image" />
	
											<ul>
												<li>
													<a href="#">
														<a href="#product-view-one" data-toggle="modal">
															<i class="bx bx-show-alt"></i>
														</a>
													</a>
												</li>
												<li>
													<a href="#">
														<i class="bx bx-heart"></i>
													</a>
												</li>
											</ul>
										</div>
			
										<h3>transform: scaleY(1);</h3>
										<span> <del>$49.00</del> $39.00</span>
	
										<a href="cart.html" class="default-btn">
											Add to cart
										</a>
									</div>
								</div>
	
								<div class="col-lg-4 col-sm-6">
									<div class="single-shop">
										<div class="shop-img">
											<img src={shopImg2} alt="Image" />
	
											<ul>
												<li>
													<a href="#">
														<a href="#product-view-one" data-toggle="modal">
															<i class="bx bx-show-alt"></i>
														</a>
													</a>
												</li>
												<li>
													<a href="#">
														<i class="bx bx-heart"></i>
													</a>
												</li>
											</ul>
										</div>
			
										<h3>EGO ias the enemy</h3>
										<span> <del>$79.00</del> $59.00</span>
	
										<a href="cart.html" class="default-btn">
											Add to cart
										</a>
									</div>
								</div>
	
								<div class="col-lg-4 col-sm-6">
									<div class="single-shop">
										<div class="shop-img">
											<img src={shopImg3} alt="Image" />
	
											<ul>
												<li>
													<a href="#">
														<a href="#product-view-one" data-toggle="modal">
															<i class="bx bx-show-alt"></i>
														</a>
													</a>
												</li>
												<li>
													<a href="#">
														<i class="bx bx-heart"></i>
													</a>
												</li>
											</ul>
										</div>
			
										<h3>Der liege augustine</h3>
										<span>$59.00</span>
	
										<a href="cart.html" class="default-btn">
											Add to cart
										</a>
									</div>
								</div>
	
								<div class="col-lg-4 col-sm-6">
									<div class="single-shop">
										<div class="shop-img">
											<img src={shopImg4} alt="Image" />
	
											<ul>
												<li>
													<a href="#">
														<a href="#product-view-one" data-toggle="modal">
															<i class="bx bx-show-alt"></i>
														</a>
													</a>
												</li>
												<li>
													<a href="#">
														<i class="bx bx-heart"></i>
													</a>
												</li>
											</ul>
										</div>
			
										<h3>Failed IT!</h3>
										<span> <del>$49.00</del> $39.00</span>
	
										<a href="cart.html" class="default-btn">
											Add to cart
										</a>
									</div>
								</div>
	
								<div class="col-lg-4 col-sm-6">
									<div class="single-shop">
										<div class="shop-img">
											<img src={shopImg5} alt="Image" />
	
											<ul>
												<li>
													<a href="#">
														<a href="#product-view-one" data-toggle="modal">
															<i class="bx bx-show-alt"></i>
														</a>
													</a>
												</li>
												<li>
													<a href="#">
														<i class="bx bx-heart"></i>
													</a>
												</li>
											</ul>
										</div>
			
										<h3>E.A.POE</h3>
										<span>$49.00</span>
	
										<a href="cart.html" class="default-btn">
											Add to cart
										</a>
									</div>
								</div>
	
								<div class="col-lg-4 col-sm-6">
									<div class="single-shop">
										<div class="shop-img">
											<img src={shopImg6} alt="Image" />
	
											<ul>
												<li>
													<a href="#">
														<a href="#product-view-one" data-toggle="modal">
															<i class="bx bx-show-alt"></i>
														</a>
													</a>
												</li>
												<li>
													<a href="#">
														<i class="bx bx-heart"></i>
													</a>
												</li>
											</ul>
										</div>
			
										<h3>Dior</h3>
										<span>$59.00</span>
	
										<a href="cart.html" class="default-btn">
											Add to cart
										</a>
									</div>
								</div>
	
								<div class="col-lg-4 col-sm-6">
									<div class="single-shop">
										<div class="shop-img">
											<img src={shopImg7} alt="Image"/>
	
											<ul>
												<li>
													<a href="#">
														<a href="#product-view-one" data-toggle="modal">
															<i class="bx bx-show-alt"></i>
														</a>
													</a>
												</li>
												<li>
													<a href="#">
														<i class="bx bx-heart"></i>
													</a>
												</li>
											</ul>
										</div>
			
										<h3>Your a heart</h3>
										<span> <del>$49.00</del> $39.00</span>
	
										<a href="cart.html" class="default-btn">
											Add to cart
										</a>
									</div>
								</div>
	
								<div class="col-lg-4 col-sm-6">
									<div class="single-shop">
										<div class="shop-img">
											<img src={shopImg8} alt="Image" />
	
											<ul>
												<li>
													<a href="#">
														<a href="#product-view-one" data-toggle="modal">
															<i class="bx bx-show-alt"></i>
														</a>
													</a>
												</li>
												<li>
													<a href="#">
														<i class="bx bx-heart"></i>
													</a>
												</li>
											</ul>
										</div>
			
										<h3>Hand cover amockup</h3>
										<span>$49.00</span>
	
										<a href="cart.html" class="default-btn">
											Add to cart
										</a>
									</div>
								</div>
	
								<div class="col-lg-4 col-sm-6 offset-sm-3 offset-lg-0">
									<div class="single-shop">
										<div class="shop-img">
											<img src={shopImg9} alt="Image" />
	
											<ul>
												<li>
													<a href="#product-view-one" data-toggle="modal">
														<i class="bx bx-show-alt"></i>
													</a>
												</li>
												<li>
													<a href="#">
														<i class="bx bx-heart"></i>
													</a>
												</li>
											</ul>
										</div>
			
										<h3>Book cover mockup</h3>
										<span>$29.00</span>
	
										<a href="cart.html" class="default-btn">
											Add to cart
										</a>
									</div>
								</div>
	
								<div class="col-lg-12 col-md-12">
									<div class="pagination-area">
										
									
										<span class="page-numbers current" aria-current="page">1</span>
										<a href="#" class="page-numbers">2</a>
										<a href="#" class="page-numbers">3</a>
										<a href="#" class="page-numbers">4</a>
										
										<a href="#" class="next page-numbers">
											<i class="bx bx-chevron-right"></i>
										</a>
									</div>
								</div>
							</div>
						</div>
					</div>

					<div class="col-lg-4">
						<div class="widget-sidebar">
							<div class="sidebar-widget search">
								<form class="search-form">
									<input class="form-control" name="search" placeholder="Search here" type="text" />
									<button class="search-button" type="submit">
										<i class="bx bx-search"></i>
									</button>
								</form>	
							</div>
	
							<div class="sidebar-widget categories">
								<h3>Categories</h3>
	
								<ul>
									<li>
										<a href="#">Education</a>
									</li>
									<li>
										<a href="#">Health Coaching</a>
									</li>
									<li>
										<a href="#">Learning</a>
									</li>
									<li>
										<a href="#">Online</a>
									</li>
									<li>
										<a href="#">Academics</a>
									</li>
									<li>
										<a href="#">Admission</a>
									</li>
									<li>
										<a href="#">Student</a>
									</li>
									<li>
										<a href="#">Graduation</a>
									</li>
								</ul>
							</div>
	
							<div class="sidebar-widget popular-post">
								<h3 class="widget-title">Popular Posts</h3>
	
								<div class="post-wrap">
									<div class="item">
										<a href="single-blog.html" class="thumb">
											<span class="fullimage cover bg1" role="img"></span>
										</a>
	
										<div class="info">
											<h4 class="title">
												<a href="single-blog.html">We will begin to explore the next two levels</a>
	
												<span class="date">20-07-2020</span>
											</h4>
										</div>
									</div>
	
									<div class="item">
										<a href="single-blog.html" class="thumb">
											<span class="fullimage cover bg2" role="img"></span>
										</a>
	
										<div class="info">
											<h4 class="title">
												<a href="single-blog.html">Determining the true goal of a good education is difficult.</a>
													
												<span class="date">19-07-2020</span>
											</h4>
										</div>
									</div>
	
									<div class="item">
										<a href="single-blog.html" class="thumb">
											<span class="fullimage cover bg3" role="img"></span>
										</a>
	
										<div class="info">
											<h4 class="title">
												<a href="single-blog.html">Implementing peer assessment in online group</a> 
													
												<span class="date">18-07-2020</span>
											</h4>
										</div>
									</div>
								</div>
							</div>
	
							<div class="sidebar-widget tags">
								<h3>Tags</h3>
	
								<ul>
									<li>
										<a href="#">Education</a>
									</li>
									<li>
										<a href="#">College</a>
									</li>
									<li>
										<a href="#">College</a>
									</li>
									<li>
										<a href="#">Math</a>
									</li>
									<li>
										<a href="#">Design</a>
									</li>
									<li>
										<a href="#">Learning</a>
									</li>
									<li>
										<a href="#">Learning</a>
									</li>
								</ul>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>

			)
		}
	}

export default ShopArea ;